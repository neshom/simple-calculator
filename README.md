# simple calculator

its a very simple calculator

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development

### requirements

you should have nodejs on your system

### Installing

```
npm i
```

## Production

```
npm run build
```

## Deployment localhost:8080

```
npm run serve
```

## Built With

- [vuetifyjs](https://vuetifyjs.com/) - Vuetify is a Vue UI Library with beautifully handcrafted Material Components. No design skills required — everything you need to create amazing applications is at your fingertips.
- [vuejs](https://vuejs.org/) - javascript framework

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details
