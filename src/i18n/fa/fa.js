export default {
  title: "مقدار عددی",
  caption: "ماشین حساب روش اول",
  num1: "مقدار اول",
  num2: "مقدار دوم",
  operator: "عملگر",
  result: "نتیجه",
  thisFieldIsNumber: "فقط مقدار عددی مجاز است",
};
